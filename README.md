### Hello! I'm Audrey Robinel - aka [sky99/sarinkhan][website] 


## Connect with me:

[<img align="left" alt="nagashur.com" width="22px" src="https://raw.githubusercontent.com/iconic/open-iconic/master/svg/globe.svg" />][website]
[<img align="left" alt="Youtube Audrey Robinel | YouTube" width="22px" src="https://cdn.jsdelivr.net/npm/simple-icons@v3/icons/youtube.svg" />][youtube]
[<img align="left" alt="Twitter arobinel | Twitter" width="22px" src="https://cdn.jsdelivr.net/npm/simple-icons@v3/icons/twitter.svg" />][twitter]
[<img align="left" alt="LinkedIn Audrey Robinel | LinkedIn" width="22px" src="https://cdn.jsdelivr.net/npm/simple-icons@v3/icons/linkedin.svg" />][linkedin]
[<img align="left" alt="instagram arobinel | Instagram" width="22px" src="https://cdn.jsdelivr.net/npm/simple-icons@v3/icons/instagram.svg" />][instagram]

<br />

---

### 📺 Latest YouTube Videos
<!-- YOUTUBE:START -->
<!-- YOUTUBE:END -->

---

### 📕 Latest Blog Posts
<!-- BLOG-POST-LIST:START -->
<!-- BLOG-POST-LIST:END -->

---


[website]: https://nagashur.com
[twitter]: https://twitter.com/arobinel
[youtube]: https://www.youtube.com/AudreyRobinel
[instagram]: https://instagram.com/arobinel
[linkedin]: https://www.linkedin.com/in/audrey-robinel-b1a08b11b/

